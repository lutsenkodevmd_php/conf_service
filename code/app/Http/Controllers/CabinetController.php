<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CabinetController extends Controller
{
    // Конструктор на проверку авторизировалься пользователь или нет
    public function __construct()
    {
        //TODO нужно сделать проверку

    }

    public function show_order(){
        $userData=Auth::user();
        $dataOrders=Order::where('user_id',$userData->id)->get();
        return view('front.userorders',['dataOrders'=>$dataOrders]);

    }
}
