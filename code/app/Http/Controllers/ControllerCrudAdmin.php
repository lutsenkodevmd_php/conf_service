<?php

namespace App\Http\Controllers;

use App\Models\EventExpert;
use App\Models\EventQuestions;
use App\Models\EventSection;
use Illuminate\Http\Request;

class ControllerCrudAdmin extends Controller
{
    public function __construct()
    {
      //  abort(404);
    }


    /**
     * Добовляем новую секцию к категории
     * @param Request $request
     * @return string
     */
    public function ajaxAddSection(Request $request){
        // Переменные которые пришли
        $name=$request->input('nameSectionNew');
        $idrecord=$request->input('idrecord');
        //Проверка на пустое имя
        if($name==""){
            return "null new name";
        }
        //Добовляем новую запись
        $modelSection=new EventSection();
        $modelSection->event_id=$idrecord;
        $modelSection->name=$name;
        $modelSection->save();
        //Получаем по этому событии все данные
        $modelSections=EventSection::where('event_id',$idrecord)->get();
        //Возвращаем все данные в виде формы
        $html=view('admincrud.component.section_ajax',
            ['modelSections'=>$modelSections])->render();
        // Ответ от ajax
        return $html;
    }


    /**
     * Добавляем критерий
     * @param Request $request
     * @return string
     */
    public function ajaxAddQuestion(Request $request){
        // Переменные которые пришли
        $name=$request->input('nameNew');
        $idrecord=$request->input('idrecord');
        //Проверка на пустое имя
        if($name==""){
            return "null new name";
        }
        //Добовляем новую запись
        $modelQuestion=new EventQuestions();
        $modelQuestion->event_id=$idrecord;
        $modelQuestion->text_questions=$name;
        $modelQuestion->save();
        //Получаем по этому событии все данные
        $modelQuestions=EventQuestions::where('event_id',$idrecord)->get();
        //Возврошаем все данные в виде формы
        $html=view('admincrud.component.questions_ajax',
            ['modelQuestions'=>$modelQuestions])->render();
        // Ответ от ajax
        return $html;
    }


    /**
     * Добавляем эксперта
     * @param Request $request
     * @return string
     */
    public function ajaxAddExpert(Request $request){
        // Переменные которые пришли
        $user_id=$request->input('user_id');
        $idrecord=$request->input('idrecord');
        //Проверка на пустое имя
        if($user_id==0){
            return "null new name";
        }

        $dataRecord=EventExpert::where('event_id',$idrecord)
            ->where('user_id',$user_id)
            ->first();
        if(is_null($dataRecord)){
            //Добавляем новую запись
            $modelQuestion=new EventExpert();
            $modelQuestion->event_id=$idrecord;
            $modelQuestion->user_id=$user_id;
            $modelQuestion->save();
        }
        //Получаем по этому событии все данные
        $modelDatas=EventExpert::where('event_id',$idrecord)->get();
        //Возвращаем все данные в виде формы
        $html=view('admincrud.component.expertusers_ajax',
            ['modelDatas'=>$modelDatas])->render();
        // Ответ от ajax
        return $html;
    }

    /**
     * Удаляем
     * @param Request $request
     * @return int|string
     */
    public function ajaxDeleteSection(Request $request){
        $idrecord=$request->input('idrecord');
        if($idrecord==""){
            return "null new name";
        }
        //Добавляем новую запись
        EventSection::where('id',$idrecord)->delete();
        // Ответ от ajax
        return 1;
    }
    /**
     * Удаляем критерий
     * @param Request $request
     * @return int|string
     */
    public function ajaxDeleteQestions(Request $request){
        $idrecord=$request->input('idrecord');
        if($idrecord==""){
            return "null new name";
        }
        //Добавляем новую запись
        EventQuestions::where('id',$idrecord)->delete();
        // Ответ от ajax
        return 1;
    }


    /**
     * Удаляем эксперта
     * @param Request $request
     * @return int|string
     */
    public function ajaxDeleteExpert(Request $request){
        $idrecord=$request->input('idrecord');
        if($idrecord==""){
            return "null new name";
        }
        //Добавляем новую запись
        EventExpert::where('id',$idrecord)->delete();
        // Ответ от ajax
        return 1;
    }


}
