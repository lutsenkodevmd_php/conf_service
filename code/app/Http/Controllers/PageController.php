<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\EventExpert;
use App\Models\ExpertAnswer;
use App\Models\Order;
use App\Models\OrderFile;
use App\Models\Page;
use App\Models\Slider;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function Symfony\Component\HttpFoundation\Session\Storage\Handler\rollback;
use function Symfony\Component\String\length;

class PageController extends Controller
{
    /**
     * Главная страница
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home(){
        $sliders=Slider::all();
        $events=Event::all();
        return view('front.home',['sliders'=>$sliders,'events'=>$events]);
    }

    /**
     * @param $id
     */
    public function showconf($id){
        $dataEvent=Event::query()->where('id',$id)
            ->first();
        if(is_null($dataEvent))
            abort(404);
        return view('front.confdata'
            ,['dataEvent'=>$dataEvent]
        );
    }

    /**
     * Отправляем заявку
     * @param Request $request
     */
    public function sendOrder(Request $request){
        //dump($request->input());

        $modelOrder=new Order();
        $modelOrder->event_id=$request->input('event_id');
        if($request->input('select_section')!="")
            $modelOrder->section=$request->input('select_section');
        $modelOrder->user_id=Auth::user()->id;
        $modelOrder->comment=$request->input('comorder');
        $modelOrder->save();
        $files=$request->file('fileorder');
        if(!is_null($files)){
                $project_image = $request->file("fileorder");
                $filename = time() . '.' . $project_image->getClientOriginalExtension();
                $fileUploads='/uploads/order/'.Auth::user()->id.'/'.$modelOrder->id.'/'.$filename;
                $destinationPath = public_path().'/uploads/order/'.Auth::user()->id.'/'.$modelOrder->id.'/' ;
                $project_image->move($destinationPath,$filename);
                $orderFile=new OrderFile();
                $orderFile->order_id=$modelOrder->id;
                $orderFile->name_file=$filename;
                $orderFile->save();
               // $datas[$key]=$fileUploads;
        }
        return redirect()->to('/sendorder_ok');
    }


    /**
     * Страница при отправке заявки
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sendorder_ok(){
        return view ('front.orderok');
    }


    /**
     * Страница регистрации
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function pageReg(){
        return view ('front.regdata');
    }

    /**
     * Страница логирования
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function pageLogin(){
        return view ('front.logdata');
    }

    /**
     * Показываем экспертизу
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function pageExpertList(){
        $user=Auth::user();
        /**
         * Получаем массив на которые нужно ответить
         */
        $eventID=EventExpert::where('user_id',$user->id)
            ->get('event_id')
            ->pluck('event_id')
            ->toArray();
        $orderIDs=Order::whereIn('event_id',$eventID)
            ->get('id')
            ->pluck('id')
            ->toArray();

        $answer=ExpertAnswer::where('user_id',$user->id)
            ->get('order_id')
            ->pluck('order_id')
            ->toArray();
        $arrayDiff=array_diff($orderIDs,$answer);

        // Данные
        $orderDatas=Order::whereIn('id',$arrayDiff)->get();
        // Показываем
        return view ('front.expertorders',['orderDatas'=>$orderDatas]);
    }


    /**
     * Отправляем ответы
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function pageExpertPost(Request $request){
        $order_id=$request->input('idorder');
        foreach ($request->input('textinput') as $key=>$value){
            $modelAnswer=new ExpertAnswer();
            $modelAnswer->order_id=$order_id;
            $modelAnswer->event_question_id=$key;
            $modelAnswer->expert_answer=$value;
            $modelAnswer->user_id=Auth::user()->id;
            $modelAnswer->save();
        }
        return redirect()->back();
    }


    /**
     * Показываем страницу
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showPage($slug){
        $dataPage=Page::where('slug',$slug)->first();
        if(is_null($dataPage))
            abort(404);
        return view ('front.page',['dataPage'=>$dataPage]);
    }

}
