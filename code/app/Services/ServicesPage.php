<?php


namespace App\Services;


use App\Models\BlogText;

class ServicesPage
{
    static function getBlogHtml($name){
        $data=BlogText::where('name',$name)->first();
        if(is_null($data))
            return "";
        return $data->html_code;
    }
}