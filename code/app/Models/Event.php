<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'desc',
        'text_about',
        'image_s',
        'image_b',
        'start_data',
        'adress',
        'reg_data',
    ];
}
