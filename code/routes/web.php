<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\CabinetController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PageController::class,'home']);
Route::get('/confdata/{id}', [PageController::class,'showconf'])->name('front.confdata');
Route::post('/sendorder',[PageController::class,'sendOrder']);
Route::get('/logindata',[PageController::class,'pageLogin']);
Route::get('/regdata',[PageController::class,'pageReg']);
Route::post('/logindata',[AuthenticatedSessionController::class,'store'])
    ->name('frobt.loginpost');
Route::post('/regdata',[RegisteredUserController::class,'store'])
    ->name('front.regpost');
Route::get('/sendorder_ok',[PageController::class,'sendorder_ok']);
Route::get('/orders',[CabinetController::class,'show_order']);

Route::get('/expert_list',[PageController::class,'pageExpertList']);
Route::post('/expert_list',[PageController::class,'pageExpertPost']);

Route::get('/page/{slug}',[PageController::class,'showPage']);




