<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('crud/ajaxnewasection',[\App\Http\Controllers\ControllerCrudAdmin::class,''])
Route::post('crud/ajaxnewasection', [\App\Http\Controllers\ControllerCrudAdmin::class, 'ajaxAddSection'])
    ->name('apicrud.ajax.newsection');

Route::post('crud/ajaxnewquestion', [\App\Http\Controllers\ControllerCrudAdmin::class, 'ajaxAddQuestion'])
    ->name('apicrud.ajax.newquestion');

Route::post('crud/ajaxnewexpert', [\App\Http\Controllers\ControllerCrudAdmin::class, 'ajaxAddExpert'])
    ->name('apicrud.ajax.ajaxnewexpert');



Route::post('crud/ajaxdeletesection', [\App\Http\Controllers\ControllerCrudAdmin::class, 'ajaxDeleteSection'])
    ->name('apicrud.ajax.deletesection');

Route::post('crud/ajaxdeletequestions', [\App\Http\Controllers\ControllerCrudAdmin::class, 'ajaxDeleteQestions'])
    ->name('apicrud.ajax.deletequestions');


Route::post('crud/ajaxdeleteexpert', [\App\Http\Controllers\ControllerCrudAdmin::class, 'ajaxDeleteExpert'])
    ->name('apicrud.ajax.deleteexpert');




