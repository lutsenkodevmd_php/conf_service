<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('expert_answers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id');
            $table->index('order_id');
            $table->bigInteger('event_question_id');
            $table->index('event_question_id');
            $table->integer('expert_answer');
            $table->bigInteger('user_id');
            $table->index('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('expert_answers');
    }
};
