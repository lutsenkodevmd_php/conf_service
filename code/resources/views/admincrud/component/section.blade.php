<input type="text" value=""
       class="input_add_section"
       placeholder="Введите название секции" data-idrecord="{{$id}}">
<div class="btn_add_section">Добавить</div>

<div class="sectiontable">

    <table class="tablecrud">
        <tr>
            <th>
                id
            </th>
            <th>
                Название
            </th>
            <th>

            </th>
        </tr>
        <?php
        $modelSections=\App\Models\EventSection::where('event_id',$id)->get();
        ?>
        @foreach($modelSections as $modelSection)
            <tr class="sectoin_{{$modelSection->id}}">
                <td>
                    {{$modelSection->id}}
                </td>
                <td>
                    {{$modelSection->name}}
                </td>
                <td>
                    <div class="delet_section" data-idrecord="{{$modelSection->id}}">Удалить</div>
                </td>
            </tr>
        @endforeach
    </table>
</div>