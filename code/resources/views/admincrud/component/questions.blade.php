<textarea class="input_add_question" data-idrecord="{{$id}}" ></textarea>
<div class="btn_add_question">Добавить</div>

<div class="questiontable">
    <table class="tablecrud">
        <tr>
            <th>
                id
            </th>
            <th>
                Критерий
            </th>
            <th>

            </th>
        </tr>
        <?php
        $modelSections=\App\Models\EventQuestions::where('event_id',$id)->get();
        ?>
        @foreach($modelSections as $modelSection)
            <tr class="questions_{{$modelSection->id}}">
                <td>
                    {{$modelSection->id}}
                </td>
                <td>
                    {{$modelSection->text_questions}}
                </td>
                <td>
                    <div class="delet_qestuoin" data-idrecord="{{$modelSection->id}}">Удалить</div>
                </td>
            </tr>
        @endforeach
    </table>
</div>