Резултаты

<?php
    $events=\App\Models\Event::all();
?>

@foreach($events as $event)
    <div class="report_event">

        <b>Название :</b> {{$event->name}} <br>
        <b>Описание :</b> {!! $event->desc !!} <br>
            <br>
        <button class="btn_show" data-id="{{$event->id}}">Результат</button>
        <div class="show_table datashow_{{$event->id}}">

            <?php
                $dataRecords=\App\Models\Order::where('event_id',$event->id)->get();
            ?>
            <table>
                <thead>
                    <tr>
                        <td>
                            id
                        </td>
                        <td>
                            Пользователь
                        </td>
                        <td>
                             Итого баллов
                        </td>
                        <td>
                            файл
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataRecords as $dataRecord)
                        <?php
                            $ballDattas=\App\Models\ExpertAnswer::where('order_id',$dataRecord->id)
                                ->get(['expert_answer']);
                            $ball=0;
                            foreach ($ballDattas as $ballDatta)
                                $ball+=$ballDatta->expert_answer;


                            $userData=\App\Models\User::where('id',$dataRecord->user_id)->first();
                        ?>
                        <tr>
                            <td>{{$dataRecord->id}}</td>
                            <td>{{$userData->name}}</td>
                            <td>{{$ball}}</td>
                            <td>
                                <?php
                                    $dataUrls=\App\Models\OrderFile::where('order_id',$dataRecord->id)->get();
                                ?>
                                @foreach($dataUrls as $dataUrl)
                                        <a href="{{ asset('uploads/order/'.$dataRecord->user_id."/".$dataRecord->id."/".$dataUrl->name_file) }}"]
                                           target="_blank"
                                        >Скачать файл {{$dataUrl->name_file}}</a>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endforeach



<style>

    .report_event{
        background: white;
        padding: 10px;
        border-radius: 10px;
        width: 100% ;
    }

    .show_table{
        display: none;
    }


    table, th, td {
        border: 1px solid black;
    }
</style>
