<div style="    width: 100%;
    display: flex;
    flex-wrap: wrap;
    align-items: center;">
    <?php
    $users=\App\Models\User::all();
    ?>
    <select class="userexpertselect" data-idrecord="{{$id}}">
        <option  value="0" selected disabled>Выберите эксперта</option>
        @foreach($users as $user)
            <option value="{{$user->id}} "> {{$user->name}} {{$user->email}}</option>
        @endforeach
    </select>

    <div class="addexpertevent">
        Добавить
    </div>
</div>


<div class="tableexpert">
    <table class="tablecrud">
        <tr>
            <th>
                id
            </th>
            <th>
                Пользователь
            </th>
            <th>

            </th>
        </tr>
        <?php
        $modelDatas=\App\Models\EventExpert::where('event_id',$id)->get();
        ?>
        @foreach($modelDatas as $modelData)
            <tr class="expert_{{$modelData->id}}">
                <td>
                    {{$modelData->id}}
                </td>
                <td>
                    {{$modelData->user->name}} {{$modelData->user->email}}
                </td>
                <td>
                    <div class="delet_expert" data-idrecord="{{$modelData->id}}">Удалить</div>
                </td>
            </tr>
        @endforeach
    </table>
</div>

