<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script src="https://cdn.ckeditor.com/4.21.0/standard/ckeditor.js"></script>

<script>

    // Изменяет поле ддя ввода информации
    CKEDITOR.replace('pages[html_code]');
    CKEDITOR.replace('blogtext[html_code]');
    CKEDITOR.replace('events[desc]');
    CKEDITOR.replace('events[text_about]');

    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.userexpertselect').select2();
    });


    /**
     * Добавить новую секцию
     */
    $(document).on('click','.btn_add_section',function () {
        var nameSectionNew=$('.input_add_section').val();
        var idrecord=$('.input_add_section').data("idrecord")
        let formData = new FormData();
        formData.append('nameSectionNew', nameSectionNew);
        formData.append('idrecord',idrecord);
        $('.sectiontable').html("load . . . . .");
        $.ajax({
            url: "{{route('apicrud.ajax.newsection')}}",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: response => {
                $('.sectiontable').html(response);
            }
        });
    })


    /**
     * Добавить новый криетрий
     */
    $(document).on('click','.btn_add_question',function () {
        var nameSectionNew=$('.input_add_question').val();
        var idrecord=$('.input_add_question').data("idrecord")
        let formData = new FormData();
        formData.append('nameNew', nameSectionNew);
        formData.append('idrecord',idrecord);
        $('.questiontable').html("load . . . . .");
        $.ajax({
            url: "{{route('apicrud.ajax.newquestion')}}",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: response => {
                $('.questiontable').html(response);
            }
        });
    })


    /**
     * Добавить новый експерта
     */
    $(document).on('click','.addexpertevent',function () {
        var user_id=$('.userexpertselect').val();
        var idrecord=$('.userexpertselect').data("idrecord")
        let formData = new FormData();
        formData.append('user_id', user_id);
        formData.append('idrecord',idrecord);
        $('.tableexpert').html("load . . . . .");
        $.ajax({
            url: "{{route('apicrud.ajax.ajaxnewexpert')}}",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: response => {
                $('.tableexpert').html(response);
            }
        });
    })



    /**
     * Удалить новую секцию
     */
    $(document).on('click','.delet_section',function () {
        var idrecord=$(this).data("idrecord")
        let formData = new FormData();
        formData.append('idrecord',idrecord);
        $('.sectoin_'+idrecord).hide();
        $.ajax({
            url: "{{route('apicrud.ajax.deletesection')}}",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: response => {
                console.log("delete");
            }
        });
    })


    /**
     * Удалить новую критерий
     */
    $(document).on('click','.delet_expert',function () {
        var idrecord=$(this).data("idrecord")
        let formData = new FormData();
        formData.append('idrecord',idrecord);
        $('.expert_'+idrecord).hide();
        $.ajax({
            url: "{{route('apicrud.ajax.deleteexpert')}}",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: response => {
                console.log("delete");
            }
        });
    })


</script>

<style>
    .btn_add_section{
        padding: 10px;
        margin-top: 10px;
        margin-bottom: 10px;
        background: green;
        color: white;
        cursor: pointer;
        width: 120px;
        text-align: center;
    }
    .btn_add_question{
        padding: 10px;
        margin-top: 10px;
        margin-bottom: 10px;
        background: green;
        color: white;
        cursor: pointer;
        width: 120px;
        text-align: center;
    }
    .tablecrud {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    .tablecrud td, .tablecrud th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    .tablecrud tr:nth-child(even){background-color: #f2f2f2;}

    .tablecrud tr:hover {background-color: #ddd;}

    .tablecrud th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
    }
    .delet_section{
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #c63b3b;
        color: white;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .delet_qestuoin{
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #c63b3b;
        color: white;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .delet_expert{
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #c63b3b;
        color: white;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
    }
    .input_add_question{
        margin-top: 40px;
    }
    .form_12_div{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .form_12_div:hover{
        background: rgba(107, 114, 128, 0.39);
    }
    .addexpertevent{
        text-align: left;
        background-color: #04AA6D;
        color: white;
        margin-left: 10px;
        padding: 10px;
        border-radius: 10px;
        cursor: pointer;
    }
    .select2-container {
        width: 370px !important;
    }
</style>

<script>
    $(document).on('click','.btn_show',function () {
        var id=$(this).data('id');
        $('.datashow_'+id).toggle();
    })
</script>