<?php

    $showExper=0;
    if(!Auth::guest()){
        $user=\Illuminate\Support\Facades\Auth::user();
        $count=\App\Models\EventExpert::where('user_id',$user->id)
            ->count();
        if($count)
            $showExper=1;
    }

?>


<html>
    <head>
        <title>Конференции и конкурсы- @yield('title')</title>

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=DM+Sans&display=swap" rel="stylesheet">

        <style>

            html{
                box-sizing: border-box;
                font-family: 'Roboto', sans-serif;
            }
            *,*::before,*::after{
                box-sizing: inherit;
                margin: 0px;
            }

            a{
                text-decoration: navajowhite;
                color: black;
            }

            /**
            *head start
            */
            .head{
                display: flex;
                flex-wrap: wrap;
                width: 100%;
                height: 60px;
                align-items: center;
                box-shadow: 0 0 10px rgba(0,0,0,0.5);
            }
            .head_wrapper{
                display: flex;
                flex-wrap: wrap;
                margin: auto;
                max-width: 1024px;
                justify-content: space-between;
                width: 100%;
            }
            .head_wrapper .navleft a{
                padding: 10px;
            }
            .loginbtn{
                background: #2d3748;
                color: white;
            }
            .regbtn{
                background: dimgray;
                color: white;
            }
            .navleft{
                display: flex;
                align-items: center;
            }
            /**
            *head end
            */

            /**
           *slider start
           */
            .sliders_list{
                display: flex;
                flex-wrap: wrap;
                width: 100%;
            }
            .wrapper_slider{
                width: 100%;
                max-width: 1024px;
                margin: auto;
            }
            .slider_img{
                width: 100%;
                object-fit: cover;
                height:600px;
                background: rgba(0, 0, 0, 0.54);
            }
            .slid_item{
                position: relative;
            }
            .div_text_slider{
                position: absolute;
                top: 190px;
                width: 100%;
                text-align: center;
            }
            .p_1{
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 700;
                font-size: 56px;
                line-height: 73px;
                text-shadow: 1px 1px 2px black, 0 0 1em black, 0 0 0.2em black;
                color: #FFFFFF;
            }
            .p_2{
                /* Font Style/Body/2 */

                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 400;
                font-size: 16px;
                line-height: 155%;
                /* or 25px */

                text-align: center;
                text-shadow: 1px 1px 2px black, 0 0 1em black, 0 0 0.2em black;
                color: #FFFFFF;

            }
            .p_3 a{
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center;
                padding: 16px 30px;
                background: #086CF9;
                border-radius: 4px;
                width: 200px;
                margin: auto;
                color: white;
                text-decoration: none;
                margin-top: 20px;
            }
            /**
           *slider end
           */



            /**
            *text_card start
             */
            .text_card{
                width: 100%;
            }
            .wrapper_text_card{
                display: flex;
                flex-wrap: wrap;
                margin: auto;
                max-width: 890px;
                justify-content: space-around;
                width: 100%;
                padding: 28px;
                position: relative;
                top: -35px;
                background: white;
                box-shadow: 0px 11px 35px rgba(0, 0, 0, 0.08);
                border-radius: 4px;
            }
            .div_i_card{
                display: flex;
                flex-wrap: wrap;
                align-items: center;
            }
            .blue_text {
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: bold;
                line-height: 155%;
                color: #086CF9;
                margin-left: 12px;
            }

            /**
            *text_card end
             */


            /**
            *MAIN blog1 end
             */
            .div_blog1{
                width: 100%;
            }
            .div_blog1wrapper{
                width: 100%;
                max-width: 1024px;
                margin: auto;
                display: flex;
                flex-wrap: wrap;
                justify-content: space-around;
                align-items: center;
            }
            .db1_text{
                width: 50%;
                padding: 10px;
            }
            .db1_image{
                width: 50%;
                padding: 10px;
            }
            .db1_image img{
                width: 100% ;
            }
            /**
          *MAIN blog1 end
           */



            /**
          *MAIN blog2 start
           */
            .div_blog2{
                width: 100%;
            }
            .div_blog2wrapper{
                width: 100%;
                max-width: 1024px;
                margin: auto;
                display: flex;
                flex-wrap: wrap;
                justify-content: space-around;
                align-items: center;
            }
            .div_100_blog2{
                width: 100%;
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 700;
                font-size: 48px;
                line-height: 62px;
                color: #222222;
                margin: auto;
                text-align: center;
                margin-top: 10px;
                margin-bottom: 23px;
            }
            .step1{
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
                flex-direction: column;
            }
            .step1 p{
                margin-top: 10px;
            }
            /**
          *MAIN blog2 end
           */


            /**
            *Kongurs end
            */
            .div_concurs{
                background: #ECF4FF;
                margin-top: 20px;
            }
            .wrapper_concurs{
                width: 100%;
                max-width: 1024px;
                margin: auto;
                display: flex;
                flex-wrap: wrap;
                justify-content: space-around;
                align-items: center;
                padding-bottom: 20px;
            }
            .title_text_concurs{
                /* Font Style/Headline/2 */
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 700;
                font-size: 48px;
                line-height: 62px;
                color: #000000;
                margin-top: 60px;
                margin-bottom: 60px;
            }
            .list_concurs{
                display: flex;
                flex-wrap: wrap;
                width: 100%;
            }
            .item_con{
                width: 30%;
            }
            .div_cont_con{
                background: white;
                padding: 5px;
                box-shadow: 0px 11px 35px rgba(0, 0, 0, 0.08);

            }
            .img_event img{
                width: 100%;
                padding: 10px;
            }
            .even_name{
                /* Font Style/Tittle/1 */

                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 700;
                font-size: 24px;
                line-height: 31px;

                color: #333333;
            }
            .even_desc{
                /* Font Style/Body/3 */
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 400;
                font-size: 14px;
                line-height: 155%;
                /* or 22px */
                color: #555555;
                margin-top: 10px;
                margin-bottom: 10px;
            }
            .even_data{
                /* Font Style/Body/3 */
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 400;
                font-size: 14px;
                line-height: 155%;
                /* or 22px */
                color: #555555;
            }
            .even_reg{
                /* Font Style/Body/3 */
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 400;
                font-size: 14px;
                line-height: 155%;
                /* or 22px */
                color: #555555;
            }



            .footer{
                display: flex;
                flex-wrap: wrap;
                margin-top: 40px;
            }
            .footer_wrapper{
                width: 100%;
                max-width: 1024px;
                margin: auto;
                display: flex;
                flex-wrap: wrap;
                justify-content: space-around;
                align-items: center;
            }

            /**
            Информации конференции
             */
            .conf_data{
                width: 100%;
            }
            .wrapper_conf{
                width: 100%;
                max-width: 1024px;
                display: flex;
                flex-wrap: wrap;
                margin: auto;
            }
            .img_conf_big{
                width: 100%;
            }
            .img_conf_big img{
                width: 100%;
                max-height: 366px;
                object-fit: cover;
            }
            .wrapper_conf .text_head{
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 400;
                font-size: 24px;
                line-height: 155%;
                color: #555555;
                width: 100%;
                text-align: center;
            }
            .wrapper_conf .text_desc{
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 400;
                font-size: 16x;
                line-height: 155%;
                color: #555555;
                width: 100%;
                text-align: center;
            }
            .wrapper_conf .text_about{
                font-family: 'DM Sans';
                font-style: normal;
                font-weight: 400;
                font-size: 16x;
                line-height: 155%;
                color: #555555;
                width: 100%;
                text-align: center;
            }

            .wraaper_form_conf{
                width: 100%;
                text-align: center;
                margin-top: 10px;
                margin-bottom: 10px;
                background: #5e5ea3a1;
                max-width: 690px;
                margin: auto;
                padding: 10px;
                border-radius: 10px;
            }
            .wr_100{
                display: flex;
                flex-wrap: wrap;
                margin: auto;
                padding: 10px;
            }
            .title_form{
                width: 200px;
            }
            .hidden_td{
                display: none;
            }

        </style>

    </head>
    <body>

        <div class="head">
        <div class="head_wrapper desc">
            <div class="logo_head">
                Logo
            </div>
            <div class="navleft">
                <a href="{{url('/')}}" >Главная</a>
                <!--
                <a >Конкурсы/конференция</a>
                -->
                <a href="{{url('/page/o_nas')}}" >О нас</a>
                <a href="{{url('/page/o_project')}}" >О проекте</a>
                <a href="{{url('/page/contact')}}">Контакты</a>



                @if (Auth::guest())
                    <a class="loginbtn" href="{{url('logindata')}}" >
                        Войти
                    </a>
                    <a class="regbtn"  href="{{url('regdata')}}" >Регистрация</a>
                @else
                    <a class="loginbtn" href="{{url('orders')}}" >
                        Личный кабинет
                    </a>


                    @if(\Illuminate\Support\Facades\Auth::user()->is_crud)

                        <a  href="{{url('/crudadmin/users')}}" >
                            CRUD меню
                        </a>
                    @endif

                    @if($showExper)
                        <a  href="{{url('/expert_list')}}" >
                            Экспертиза
                        </a>
                    @endif



                    <form method="POST" action="{{ route('logout') }}">
                        <div class="button"   onclick="event.preventDefault();
                                                this.closest('form').submit();">
                            @csrf
                            Выход
                        </div>
                    </form>

                @endif



            </div>
        </div>
    </div>

        @yield('content')

        <div class="footer">
            <div class="footer_wrapper">
                2023 г.
            </div>
        </div>

        @yield('jsfooter')
    </body>
</html>