
@extends('front.app')

@section('title', 'Результат отправки')

@section('content')
    <div class="conf_data">
        <div class="wrapper_conf">
            <h2 style="width: 100%">Список заявок на экспертизу</h2><br>

            <table border="1">
                <thead>
                    <th>id</th>
                    <th>Конференция/конкурс</th>
                    <th>Секция</th>
                    <th>Дата отправки</th>
                    <th>Экспертиза</th>
                </thead>
                <tbody>
                    @foreach($orderDatas as $dataOrder)
                        <tr>
                            <td>{{$dataOrder->id}}</td>
                            <td>{{$dataOrder->event->name}}</td>
                            <td>{{$dataOrder->eventsection->name}}</td>
                            <td>{{$dataOrder->created_at}}</td>
                            <td class="show_expert" data-id="{{$dataOrder->id}}">Произвести экспертизу</td>
                        </tr>
                        <tr class="order_{{$dataOrder->id}} hidden_td">
                            <td colspan="5">
                                <div class="wrapper_answer">
                                    <div class="wr_100">
                                        Файл
                                    </div>
                                    <div class="wr_100">
                                        <?php
                                        $filesGet=\App\Models\OrderFile::where('order_id',$dataOrder->id)->get();
                                        ?>
                                        @foreach($filesGet as $file)
                                            <p><a href="{{ asset('uploads/order/'.$dataOrder->user_id."/".$dataOrder->id."/".$file->name_file) }}"]
                                                  target="_blank"
                                                >Скачать файл {{$file->name_file}}</a> </p>
                                        @endforeach
                                    </div>
                                    <div class="q_list">
                                       <p>
                                           Опрос
                                       </p>
                                        <?php
                                          $dataQs=\App\Models\EventQuestions::where('event_id',$dataOrder->event->id)
                                              ->get();
                                         // dump($dataQs);
                                        ?>
                                        <form method="post">
                                        @foreach($dataQs as $dataQ)
                                            <p>
                                                {{$dataQ->text_questions}}
                                                <input type="text" name="textinput[{{$dataQ->id}}]" value="">
                                            </p>
                                        @endforeach
                                            <p>
                                                <input type="submit" value="Ответить">
                                            </p>
                                            @csrf()
                                            <input type="hidden" name="idorder" value="{{$dataOrder->id}}">
                                        </form>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('jsfooter')
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw==" crossorigin="anonymous" referrerpolicy="no-referrer" />


    <script>
        $(document).ready(function(){
            $('.show_expert').click(function () {
                var dataID=$(this).data('id');
                if($('.order_'+dataID).hasClass('hidden_td')){
                    $('.order_'+dataID).removeClass('hidden_td');
                }else{
                    $('.order_'+dataID).addClass('hidden_td');
                }

            })
        });
    </script>
@endsection