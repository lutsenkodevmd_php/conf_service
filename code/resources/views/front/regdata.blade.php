
@extends('front.app')

@section('title', 'Регистрация')



@section('content')

    <div class="conf_data">
        <div class="wrapper_conf">
            <div class="wr_100">
            <h2 style="width: 100%">Регистрация нового пользователя</h2>
            <br>
            </div>
            <form  style="width: 100%"  action="{{ route('front.regpost') }}" method="post">
                <div class="wr_100">
                    <div class="title_form">
                        email
                    </div>
                    <div class="type_form">
                        <input type="text" name="email">
                    </div>
                </div>
                <div class="wr_100">
                    <div class="title_form">
                        Имя
                    </div>
                    <div class="type_form">
                        <input type="text" name="name">
                    </div>
                </div>
                <div class="wr_100">
                    <div class="title_form">
                        Пароль
                    </div>
                    <div class="type_form">
                        <input type="password" name="password">
                    </div>
                </div>
                <div class="wr_100">
                    <input type="submit" value="Войти">
                    @csrf()
                </div>
            </form>
        </div>
    </div>
@endsection

@section('jsfooter')

@endsection