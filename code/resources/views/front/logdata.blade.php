
@extends('front.app')

@section('title', 'Войти')



@section('content')

    <div class="conf_data">
        <div class="wrapper_conf">

            <h2 style="width: 100%">Войт в кабинет</h2><br>
            <form style="width: 100%"  action="{{ route('frobt.loginpost') }}" method="post">
                <div class="wr_100">
                    <div class="title_form">
                        email
                    </div>
                    <div class="type_form">
                        <input type="text" name="email">
                    </div>
                </div>
                <div class="wr_100">
                    <div class="title_form">
                        Пароль
                    </div>
                    <div class="type_form">
                        <input type="password" name="password">
                    </div>
                </div>
                <div class="wr_100">
                    <input type="submit" value="Войти">
                    @csrf()
                </div>
            </form>
        </div>
    </div>
@endsection

@section('jsfooter')

@endsection