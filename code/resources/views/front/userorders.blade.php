
@extends('front.app')

@section('title', 'Результат отправки')

@section('content')
    <div class="conf_data">
        <div class="wrapper_conf">
            <h2 style="width: 100%">Список заявок отправленные пользователем</h2><br>

            <table>
                <thead>
                    <th>id</th>
                    <th>Конференция/конкурс</th>
                    <th>Секция</th>
                    <th>Дата отправки</th>
                </thead>
                <tbody>
                    @foreach($dataOrders as $dataOrder)
                        <tr>
                            <td>{{$dataOrder->id}}</td>
                            <td>{{$dataOrder->event->name}}</td>
                            <td>{{$dataOrder->eventsection->name}}</td>
                            <td>{{$dataOrder->created_at}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('jsfooter')

@endsection