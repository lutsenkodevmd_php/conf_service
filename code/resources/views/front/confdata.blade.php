
@extends('front.app')

@section('title', 'Информация о конференции')



@section('content')

    <div class="conf_data">
        <div class="wrapper_conf">
            <div class="img_conf_big">
                <img src="{{asset($dataEvent->image_b)}}" >
            </div>
            <div class="text_head">
                {{$dataEvent->name}}
            </div>
            <div class="text_desc">
                {!!  $dataEvent->desc!!}
            </div>
            <div class="text_about">
                {!!  $dataEvent->text_about!!}
            </div>
            <div class="text_about">
               <b> Дата проведения : </b> {!!  $dataEvent->start_data!!}
            </div>
            <div class="text_about">
                <b>Дата заверщения подачи заявки :</b> {!!  $dataEvent->reg_data!!}
            </div>
            <div class="text_about">
                <b> Адресс проведения :</b> {!!  $dataEvent->adress!!}
            </div>


            <div class="wraaper_form_conf">
                @if (Auth::guest())
                    Вам нужно пройти регистрацию для подачи заявки

                @else
                    Вы можете отправить заявку
                <form method="post"
                      enctype="multipart/form-data"
                      action="{{url('sendorder')}}">
                    <div class="wr_100">
                        <div class="title_form">
                            Файл
                        </div>
                        <div class="type_form">
                            <input type="file" name="fileorder">
                        </div>

                    </div>
                    <div class="wr_100">
                        <div class="title_form">
                            Комментарии
                        </div>
                        <div class="type_form">
                            <textarea name="comorder"></textarea>
                        </div>
                    </div>
                    <?php
                        $dataSection=\App\Models\EventSection::query()
                            ->where('event_id',$dataEvent->id)
                            ->get()
                    ?>
                    @if(count($dataSection))
                        <div class="wr_100">
                            <div class="title_form">
                                Секция
                            </div>
                            <div class="type_form">
                                <select name="select_section">
                                    <option selected value="0">-</option>
                                    @foreach($dataSection as $dataSection)
                                    <option value="{{$dataSection->id}}">
                                        {{$dataSection->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    @endif
                    <div class="wr_100">
                        <input type="submit" value="Отправить">
                        @csrf
                        <input type="hidden" name="event_id" value="{{$dataEvent->id}}">
                    </div>
                </form>

                @endif


            </div>

        </div>
    </div>
@endsection

@section('jsfooter')

@endsection