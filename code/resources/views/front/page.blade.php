
@extends('front.app')

@section('title', $dataPage->title)

@section('content')
    <div class="conf_data">
        <div class="wrapper_conf">
            <h2 style="width: 100%">{{$dataPage->title}}</h2><br>

            {!! $dataPage->html_code  !!}
        </div>
    </div>
@endsection

@section('jsfooter')

@endsection