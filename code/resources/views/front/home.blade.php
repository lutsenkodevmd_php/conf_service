
@extends('front.app')

@section('title', 'Главная страница')



@section('content')
    <div class="sliders_list">
        <div class="wrapper_slider">
            @foreach($sliders as $slider)
                <div class="slid_item">
                    <img
                            class="slider_img"
                            src="{{asset($slider->image_file)}}"
                    >
                    <div class="div_text_slider">
                        <div class="p_1">{{$slider->text_title}}</div>
                        <div class="p_2">{{$slider->text_about}}</div>
                        <div class="p_3">
                            <a href="{{$slider->url_btn}}">
                                {{$slider->text_btn}}
                            </a>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    </div>


    <div class="text_card">
        <div class="wrapper_text_card">
            <div class="div_i_card">
                <div class="div_image">
                    <img src="{{asset('images/png/conf_ico.png')}}">
                </div>
                <div class="blue_text">
                    Все конференции
                </div>
            </div>
            <div class="div_i_card">
                <div class="div_image">
                    <img src="{{asset('images/png/expert.png')}}">
                </div>
                <div class="blue_text">
                    Только эксперты
                </div>
            </div>
            <div class="div_i_card">
                <div class="div_image">
                    <img src="{{asset('images/png/send.png')}}">
                </div>
                <div class="blue_text">
                    Развивайся
                </div>
            </div>
        </div>
    </div>

    <div class="div_blog1">
        <div class="div_blog1wrapper">
            <div class="db1_text">
                {!! \App\Services\ServicesPage::getBlogHtml("blog_onas_index") !!}
            </div>
            <div class="db1_image">
                <img src="{{ asset('/images/png/blog1.png') }}"
                >
            </div>
        </div>
    </div>


    <div class="div_blog2">
        <div class="div_blog2wrapper">
            <div class="div_100_blog2">
                Как это работает
            </div>
            <div class="step1">
                <img src="{{asset('images/png/step1.png')}}">
                <p>Шаг 1.Регистрация</p>
            </div>
            <div class="step1">
                <img src="{{asset('images/png/step2.png')}}">
                <p>Шаг 2.Выбор</p>
            </div>
            <div class="step1">
                <img src="{{asset('images/png/step3.png')}}">
                <p>Шаг 3.Отправка</p>
            </div>
            <div class="step1">
                <img src="{{asset('images/png/step4.png')}}">
                <p>Шаг 4.Результат</p>
            </div>
        </div>
    </div>

    <div class="div_concurs">
        <div class="wrapper_concurs">
            <div class="title_text_concurs">
                Конференции / конкурсы
            </div>

            <div class="list_concurs">

                @foreach($events as $event)
                    <div class="item_con">
                        <div class="div_cont_con">
                            <div class="img_event">
                                <img src="{{asset($event->image_s)}}">
                            </div>
                            <div class="even_name">
                                <a href="{{route('front.confdata',['id'=>$event->id])}}">
                                    {{$event->name}}
                                </a>

                            </div>
                            <div class="even_desc">
                                {!! $event->desc !!}
                            </div>
                            <div class="even_data">
                                Дата проведения : {{  $event->start_data }}
                            </div>
                            <div class="even_reg">
                                Прием : {{  $event->reg_data }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
@endsection

@section('jsfooter')
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw==" crossorigin="anonymous" referrerpolicy="no-referrer" />


    <script>
        $(document).ready(function(){
            $('.wrapper_slider').slick({
                infinite: true,
                dots: false,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        });
    </script>
@endsection