<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Пользователи
    |--------------------------------------------------------------------------
    |
    | Конфигуарция БД пользователя
    */


    // Путь к модели
    'model'=>'App\\Models\\User',




    /**
     * Страница списка
     */
    'listTitle'=>'Список пользователей',
    'list' => [
        'id'=>[
            'title'=>'ID',
            'type'=>'text',
        ],
        'name'=>[
            'title'=>'Ф.И.О.',
            'type'=>'text',
        ],
        'email'=>[
            'title'=>'email',
            'type'=>'text',
        ],
        'is_crud'=>[
            'title'=>'Вход в AdminCRUD',
            'type'=>'boolean',
        ],
    ],
    //какие действия при показе всех форм
    'list_event'=>['edit','delete'],

    // показываем форму для добавления
    'createTitle'=>'Создать пользователя',
    'create'=>[
        'name'=>[
            'title'=>'Ф.И.О.',
            'type'=>'text',
        ],
        'email'=>[
            'title'=>'email',
            'type'=>'email',
        ],
        'password'=>[
            'title'=>'Пароль',
            'type'=>'password',
        ],
        'is_crud'=>[
            'title'=>'Вход в AdminCRUD',
            'type'=>'boolean',
        ],
    ],

    // показываем форму для редактирования
    'editTitle'=>'Редактировать пользователя',
    'edit'=>[
        'name'=>[
            'title'=>'Ф.И.О.1',
            'type'=>'text',
        ],
        'email'=>[
            'title'=>'email',
            'type'=>'email',
        ],
        'is_crud'=>[
            'title'=>'Вход в AdminCRUD',
            'type'=>'boolean',
        ],
    ]
];
