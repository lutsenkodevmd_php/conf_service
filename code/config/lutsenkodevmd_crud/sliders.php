<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Пользователи
    |--------------------------------------------------------------------------
    |
    | Конфигуарция БД пользователя
    */


    // Путь к модели
    'model'=>'App\\Models\\Slider',




    /**
     * Страница списка
     */
    'listTitle'=>'Список слайдов',
    'list' => [
        'id'=>[
            'title'=>'ID',
            'type'=>'text',
        ],
        'name'=>[
            'title'=>'Название',
            'type'=>'text',
        ],
    ],
    //какие действия при показе всех форм
    'list_event'=>['edit','delete'],

    // показываем форму для добавления
    'createTitle'=>'Создать слайд',
    'create'=>[
        'name'=>[
            'title'=>'Название',
            'type'=>'text',
        ],
        'text_title'=>[
            'title'=>'Основной текст',
            'type'=>'text',
        ],
        'text_about'=>[
            'title'=>'Остальной текст',
            'type'=>'text',
        ],
        'text_btn'=>[
            'title'=>'Текст кнопки',
            'type'=>'text',
        ],
        'url_btn'=>[
            'title'=>'url кнопки',
            'type'=>'text',
        ],
        'image_file'=>[
            'title'=>'Файл',
            'puth'=>"slider",
            'type'=>"file",
        ],

    ],

    // показываем форму для редактирования
    'editTitle'=>'Редактировать слайд',
    'edit'=>[
        'name'=>[
            'title'=>'Название',
            'type'=>'text',
        ],
        'text_title'=>[
            'title'=>'Основной текст',
            'type'=>'text',
        ],
        'text_about'=>[
            'title'=>'Остальной текст',
            'type'=>'text',
        ],
        'text_btn'=>[
            'title'=>'Текст кнопки',
            'type'=>'text',
        ],
        'url_btn'=>[
            'title'=>'url кнопки',
            'type'=>'text',
        ],
        'image_file'=>[
            'title'=>'Файл',
            'puth'=>"slider",
            'type'=>"file",
        ],

    ]
];
