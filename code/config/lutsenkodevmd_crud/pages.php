<?php
return [
    // Путь к модели
    'model'=>'App\\Models\\Page',
    /**
     * Страница списка
     */
    'listTitle'=>'Список страниц',
    'list' => [
        'id'=>[
            'title'=>'ID',
            'type'=>'text',
        ],
        'title'=>[
            'title'=>'Название страницы',
            'type'=>'text',
        ],
        'slug'=>[
            'title'=>'ЧПУ страницы',
            'type'=>'text',
        ],
    ],
    //какие действия при показе всех форм
    'list_event'=>['edit','delete'],
    // показываем форму для добавления
    'createTitle'=>'Создать страницу',
    'create'=>[
        'title'=>[
            'title'=>'Название',
            'type'=>'text',
        ],
        'slug'=>[
            'title'=>'ЧПУ страницы',
            'type'=>'text',
        ],
        'html_code'=>[
            'title'=>'HTML код страницы',
            'type'=>'textarea',
        ],

    ],
    // показываем форму для редактирования
    'editTitle'=>'Редактировать страницу',
    'edit'=>[
        'title'=>[
            'title'=>'Название',
            'type'=>'text',
        ],
        'slug'=>[
            'title'=>'ЧПУ страницы',
            'type'=>'text',
        ],
        'html_code'=>[
            'title'=>'HTML код страницы',
            'type'=>'textarea',
        ],
    ]
];
