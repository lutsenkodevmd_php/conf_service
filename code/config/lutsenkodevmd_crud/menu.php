<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Меню админ
    |--------------------------------------------------------------------------
    |
    | Конфигуарция меню
    */
    'components' => [
        [
            'type'=>"head",
            'title'=>"Компоненты",
            'submenu' => [
                'users' =>[
                    'title'=>'Пользователи',
                    'type'=>'crud',
                ],
                'sliders' =>[
                    'title'=>'Слайд главной страницы',
                    'type'=>'crud',
                ],
                'pages' =>[
                    'title'=>'Страницы сайта',
                    'type'=>'crud',
                ],
                'blogtext' =>[
                    'title'=>'html блок',
                    'type'=>'crud',
                ],
                'events' =>[
                    'title'=>'Мероприятия',
                    'type'=>'crud',
                ],
                'reportpage' =>[
                    'title'=>'Результат',
                    'type'=>'pageсomponent',
                ],
            ],
        ],
    ],







];
