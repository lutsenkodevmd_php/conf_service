<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Пользователи
    |--------------------------------------------------------------------------
    |
    | Конфигуарция БД пользователя
    */


    // Путь к модели
    'model'=>'App\\Models\\Event',
    /**
     * Страница списка
     */
    'listTitle'=>'Список мероприятий/конкурсов/конференций',
    'list' => [
        'id'=>[
            'title'=>'ID',
            'type'=>'text',
        ],
        'name'=>[
            'title'=>'Название',
            'type'=>'text',
        ],
        'start_data'=>[
            'title'=>'Дата проведения',
            'type'=>'text',
        ],
        'reg_data'=>[
            'title'=>'Дата регистрации(до)',
            'type'=>'text',
        ],
    ],
    //какие действия при показе всех форм
    'list_event'=>['edit','delete'],
    // показываем форму для добавления
    'createTitle'=>'Создать мероприятия/конкурс/конференцию',
    'create'=>[
        'name'=>[
            'title'=>'Название',
            'type'=>'text',
        ],
        'desc'=>[
            'title'=>'Мини описание',
            'type'=>'textarea',
        ],
        'text_about'=>[
            'title'=>'Основное описание',
            'type'=>'textarea',
        ],
        'text_about'=>[
            'title'=>'Остальное описание',
            'type'=>'textarea',
        ],
        'image_s'=>[
            'title'=>'Превью картинка',
            'puth'=>"image_s",
            'type'=>"file",
        ],
        'image_b'=>[
            'title'=>'Основная картинка',
            'puth'=>"image_s",
            'type'=>"file",
        ],
        'image_b'=>[
            'title'=>'Основная картинка',
            'puth'=>"image_s",
            'type'=>"file",
        ],
        'start_data'=>[
            'title'=>'Дата проведения',
            'type'=>"date",
        ],
        'reg_data'=>[
            'title'=>'Дата регистрации',
            'type'=>"date",
        ],
        'adress'=>[
            'title'=>'Адрес проведения',
            'type'=>'text',
        ],
        
    ],
    // показываем форму для редактирования
    'editTitle'=>'Редактировать мероприятие/конкурс/конференцию',
    'edit'=>[
        'name'=>[
            'title'=>'Название',
            'type'=>'text',
        ],
        'desc'=>[
            'title'=>'Мини описание',
            'type'=>'textarea',
        ],
        'text_about'=>[
            'title'=>'Основное описание',
            'type'=>'textarea',
        ],
        'text_about'=>[
            'title'=>'Остальное описание',
            'type'=>'textarea',
        ],
        'image_s'=>[
            'title'=>'Превью картинка',
            'puth'=>"image_s",
            'type'=>"file",
        ],
        'image_b'=>[
            'title'=>'Основная картинка',
            'puth'=>"image_s",
            'type'=>"file",
        ],
        'image_b'=>[
            'title'=>'Основная картинка',
            'puth'=>"image_s",
            'type'=>"file",
        ],
        'start_data'=>[
            'title'=>'Дата проведения',
            'type'=>"date",
        ],
        'reg_data'=>[
            'title'=>'Дата регистрации',
            'type'=>"date",
        ],
        'adress'=>[
            'title'=>'Адрес проведения',
            'type'=>'text',
        ],
        'section'=>[
            'title'=>'Секции',
            'namecomponent'=>"section",
            'type'=>"component",
        ],
        'questions'=>[
            'title'=>'Критерии для эксперта',
            'namecomponent'=>"questions",
            'type'=>"component",
        ],
        'experts'=>[
            'title'=>'Эксперты',
            'namecomponent'=>"expertusers",
            'type'=>"component",
        ],

    ]
];
